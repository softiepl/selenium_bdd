package pageobjects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage  extends BasePage {
//    private WebDriver driver;
//    private WebDriverWait wait;

    @FindBy(id = "email")
    private WebElement loginField;

    @FindBy(id = "passwd")
    private WebElement passwordField;

    @FindBy(id = "email_create")
    private WebElement email_create;

    @FindBy(id = "SubmitCreate")
    private WebElement submitCreate;

    @FindBy(className = "alert-danger")
    private WebElement authAlert;

    @FindBy(linkText = "Forgot your password?")
    private WebElement forgotPasswordLink;

    @FindBy(id = "SubmitLogin")
    private WebElement submitLoginButton;


    public LoginPage(WebDriver driver, WebDriverWait wait){
        super(driver,wait);
    }

    public void login(String email, String password) {
        loginField.sendKeys(email);
        passwordField.sendKeys(password);
        submitLoginButton.click();
    }

    public RegisterPage goToRegister(String email){
        email_create.sendKeys(email);
        submitCreate.click();
        return new RegisterPage(email, driver, wait);
    }

    public boolean isEmailRequiredAlertDisplayed(){
        wait.until(ExpectedConditions.visibilityOf(authAlert));
        return authAlert.getText().contains("An email address required");
    }

    public boolean isInvalidEmailAlertDisplayed(){
        wait.until(ExpectedConditions.visibilityOf(authAlert));
        return authAlert.getText().contains("Invalid email address");
    }

    public boolean isPasswordIsRequiredAlertDisplayed(){
        wait.until(ExpectedConditions.visibilityOf(authAlert));
        return authAlert.getText().contains("Password is required");
    }

    public boolean isAuthenticationFailedAlertDisplayed(){
        wait.until(ExpectedConditions.visibilityOf(authAlert));
        return authAlert.getText().contains("Authentication failed");
    }

    public boolean isAuthAlertDisplayed(){
        wait.until(ExpectedConditions.visibilityOf(authAlert));
        return authAlert.isDisplayed();
    }

    public ForgotPasswordPage goToForgotPasswordPage() {
        wait.until(ExpectedConditions.elementToBeClickable(forgotPasswordLink));
        forgotPasswordLink.click();
        return new ForgotPasswordPage(driver, wait);
    }
}

package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.HomePage;
import pageobjects.LoginPage;

public class LoginTest extends BaseTest {


    @Test
    void shouldDispalyInvalidEmailAlertWrongEmailIsProvided() {
        HomePage homePage = new HomePage(driver, wait);
        homePage.open();
        LoginPage loginPage = homePage.goToLogin();
        loginPage.login("test", "wrongpass");
        Assertions.assertTrue(loginPage.isInvalidEmailAlertDisplayed());
    }

    @Test
    void shouldDispalyPasswordIsRequiredWhenNoPasswordIsProvided() {
        HomePage homePage = new HomePage(driver, wait);
        homePage.open();
        LoginPage loginPage = homePage.goToLogin();
        loginPage.login("test@softie.pl", "");
        Assertions.assertTrue(loginPage.isPasswordIsRequiredAlertDisplayed());
    }

    @Test
    void shouldDispalyAuthenticationFailedWhenWrongCredentialsAreProvided() {
        HomePage homePage = new HomePage(driver, wait);
        homePage.open();
        LoginPage loginPage = homePage.goToLogin();
        loginPage.login("test@softie.pl", "wrongPass");
        Assertions.assertTrue(loginPage.isAuthenticationFailedAlertDisplayed());
    }

    @Test
    void shouldDispalyEmailRequiredAlertWhenNoEmailIsProvided() {
        HomePage homePage = new HomePage(driver, wait);
        homePage.open();
        LoginPage loginPage = homePage.goToLogin();
        loginPage.login("", "");
        Assertions.assertTrue(loginPage.isEmailRequiredAlertDisplayed());
    }

    @Test
    void shouldLoginUserWhenCorrectCredentialsAreProvided() {
        HomePage homePage = new HomePage(driver, wait);
        homePage.open();
        LoginPage loginPage = homePage.goToLogin();
        loginPage.login("test@softie.pl", "1qaz!QAZ");
        Assertions.assertTrue(loginPage.isSignOutButtonDisplayed());
    }

}

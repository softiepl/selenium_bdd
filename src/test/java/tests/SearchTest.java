package tests;

import org.junit.jupiter.api.Test;
import pageobjects.BasePage;
import pageobjects.LoginPage;
import pageobjects.SearchResultsPage;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SearchTest extends BaseTest {

    @Test
    void shouldReturnNonEmptySearchResultsWhenLookingForExistingProduct() {
        BasePage basePage = new BasePage(driver, wait);
        basePage.open();
        SearchResultsPage searchResultsPage = basePage.searchFor("Dress");
        assertTrue(searchResultsPage.getSearchResultsProductsNumber() > 0);
    }

    @Test
    void shouldReturnEmptySearchResultsWhenLookingForNonExistingProduct() {
        BasePage basePage = new BasePage(driver, wait);
        basePage.open();
        SearchResultsPage searchResultsPage = basePage.searchFor("Drill");
        assertEquals("0 results have been found.", searchResultsPage.getSearchResultsSummaryText());
        assertTrue(searchResultsPage.getSearchResultsProductsNumber() == 0);
    }
    @Test
    void shouldReturnEmptySearchResultsWhenLookingForNonExistingProduct2L() {
        BasePage basePage = new BasePage(driver, wait);
        basePage.open();
        SearchResultsPage searchResultsPage = basePage.searchFor("t-shirt");
        assertEquals("0 results have been found.", searchResultsPage.getSearchResultsSummaryText());
        assertTrue(searchResultsPage.getSearchResultsProductsNumber() == 1);
    }

}

package tests;

import org.junit.jupiter.api.Test;
import pageobjects.ForgotPasswordPage;
import pageobjects.HomePage;
import pageobjects.LoginPage;

import static org.junit.jupiter.api.Assertions.*;

public class ForgotPasswordTest extends BaseTest {

    @Test
    void shouldDisplayInvalidEmailAddressErrorWhenIncorrectSyntaxEmailProvided() {
        HomePage homePage = new HomePage(driver, wait);
        homePage.open();
        LoginPage loginPage = homePage.goToLogin();
        ForgotPasswordPage forgotPasswordPage = loginPage.goToForgotPasswordPage();
        forgotPasswordPage.retrivePasswordForEmail("wrong@email@syntax@com");
        assertEquals("There is 1 error", forgotPasswordPage.getErrorMessageMainText());
        assertEquals("Invalid email address.", forgotPasswordPage.getErrorMessageDetailsText());
    }

    @Test
    void shouldDisplayNoAccountRegisteredForThisEmailWhenUnregisteredEmailProvided() {
        HomePage homePage = new HomePage(driver, wait);
        homePage.open();
        LoginPage loginPage = homePage.goToLogin();
        ForgotPasswordPage forgotPasswordPage = loginPage.goToForgotPasswordPage();
        forgotPasswordPage.retrivePasswordForEmail("thisemailnotexistindb@gmail.com");
        assertEquals("There is 1 error", forgotPasswordPage.getErrorMessageMainText());
        assertEquals("There is no account registered for this email address.", forgotPasswordPage.getErrorMessageDetailsText());
    }

    @Test
    void shouldDisplaySuccsessAlertWhenRegisteredEmailProvided() {
        HomePage homePage = new HomePage(driver, wait);
        homePage.open();
        LoginPage loginPage = homePage.goToLogin();
        ForgotPasswordPage forgotPasswordPage = loginPage.goToForgotPasswordPage();
        forgotPasswordPage.retrivePasswordForEmail("test@softie.pl");
        assertEquals("A confirmation email has been sent to your address: test@softie.pl", forgotPasswordPage.getSuccessAlertText());
    }
}

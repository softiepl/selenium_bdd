package tests;

import org.junit.jupiter.api.Test;
import pageobjects.ShoppingPage;
import pageobjects.HomePage;
import pageobjects.LoginPage;

import static org.junit.jupiter.api.Assertions.*;

public class ShoppingTest extends BaseTest {

    @Test
    void shouldCorrectlyUpdateBasketValueWhenAddingProducts() {
        ShoppingPage shoppingPage = new ShoppingPage(driver, wait);
        shoppingPage.open();
        shoppingPage.addRandomProductsToTheBasket(3);
        assertEquals(shoppingPage.getExpectedBasketValue(), shoppingPage.getCurrentBasketValue());
    }

    @Test
    void shouldPlaceAnOrderForAddedProducts() {
        HomePage homePage = new HomePage(driver, wait);
        homePage.open();

        LoginPage loginPage = homePage.goToLogin();
        loginPage.login("test@softie.pl", "1qaz!QAZ");

        ShoppingPage shoppingPage = loginPage.goShoppingPage();
        shoppingPage.addRandomProductsToTheBasket(3);
        shoppingPage.placeOrderForCurrentBasket();

        assertTrue(shoppingPage.getOrderDetailsBoxText().contains("$" + shoppingPage.getExpectedBasketValue()));
    }


}
